package az.raheddin.ms4demo2d.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TicTacToeTest {

    @Captor
    ArgumentCaptor<TicTacToeBean> ticTacToeBeanArgumentCaptor;

    @Mock
    private ITicTacToeDAO dao;

    @InjectMocks
    private TicTacToe ticTacToe ;

    @Test
    public void whenXOutsideBoardThenRuntimeException() {
        //Arrange
        int x = 5; //is outside of the board
        int y = 2;

        //Act & Arrange
        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside board");
    }

    @Test
    public void whenOccupiedThenRuntimeException() {
        //Arrange
        when(dao.getLastMove()).thenReturn(new TicTacToeBean(1,'X',1,2));

        int x = 2;
        int y = 1;

        ticTacToe.play(x, y);

        //Act & Arrange
        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");
    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX() {
        assertThat(ticTacToe.nextPlayer()).isEqualTo('X');
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThenO() {
        //Arrange
        when(dao.getLastMove()).thenReturn(new TicTacToeBean(1,'X',1,2));
        //when()

        //Act
        ticTacToe.play(1, 1); //Player is X

        //Assart
        assertThat(ticTacToe.nextPlayer()).isEqualTo('O'); //O

        //verify(dao,times(1)).saveMove(any());
        //verify(dao,atLeast(0)).saveMove(any());
        verify(dao,times(1)).saveMove(new TicTacToeBean(2,'X',1,1));
        verify(dao,times(1)).saveMove(ticTacToeBeanArgumentCaptor.capture());

        TicTacToeBean value = ticTacToeBeanArgumentCaptor.getValue();
        assertThat(value).isEqualTo(new TicTacToeBean(2,'X',1,1));
    }

    @Test
    public void whenPlayThenNoWinner() {
        when(dao.getLastMove()).thenReturn(new TicTacToeBean(1,'X',1,2));
        String actual = ticTacToe.play(1, 1);
        assertThat(actual).isEqualTo("No winner"); //O
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        when(dao.getLastMove()).thenReturn(new TicTacToeBean(1,'X',1,2));
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 1); // X
        ticTacToe.play(2, 2); // O

        String actual = ticTacToe.play(3, 1); // X

        assertThat(actual).isEqualTo("X is the winner"); //O
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        when(dao.getLastMove()).thenReturn(new TicTacToeBean(1,'X',1,2));
        ticTacToe.play(2, 1); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(3, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        String actual = ticTacToe.play(1, 3); // O
        assertThat(actual).isEqualTo("O is the winner");
    }
    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        //Arrange
        when(dao.getLastMove()).thenReturn(new TicTacToeBean(1,'X',1,2));

        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 3); // O
        String actual = ticTacToe.play(3, 3); // O
        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndBottomTopDiagonalLineThenWinner() {
        when(dao.getLastMove()).thenReturn(new TicTacToeBean(1,'X',1,2));
        ticTacToe.play(1, 3); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 2); // O
        String actual = ticTacToe.play(3, 1); // O
        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        when(dao.getLastMove()).thenReturn(new TicTacToeBean(1,'X',1,2));
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        ticTacToe.play(1, 3);
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
        ticTacToe.play(2, 2);
        ticTacToe.play(3, 1);
        ticTacToe.play(3, 3);
        String actual = ticTacToe.play(3, 2);
        assertThat(actual).isEqualTo("The result is draw");
    }


}