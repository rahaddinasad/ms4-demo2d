package az.raheddin.ms4demo2d.service;

public class TicTacToeDAOFake implements ITicTacToeDAO{
    @Override
    public TicTacToeBean getLastMove() {
        System.out.println("This is Fake impl getLastMove");
        return new TicTacToeBean(1,'X',1,2);
    }

    @Override
    public void saveMove(TicTacToeBean ticTacToeBean) {
        System.out.println("This is Fake impl saveMove");
     //save
    }
}
