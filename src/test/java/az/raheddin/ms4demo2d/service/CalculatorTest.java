package az.raheddin.ms4demo2d.service;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CalculatorTest {



    private Calculator calculator =new Calculator();


    //Supports add operation
    //As a user , ehrn I enter 6 and 5 as input , the result be 11
    //AAA style : Arrange,Act,Assert
    @Test
    public void gibnAandBWhenAddTennC(){
     //Arrange
        int a=5;
        int b=6;
        int c=11;

    //Act
    int result =calculator.add(a,b);

    //Assert
    assertThat(result).isEqualTo(c);

    }

    //Supports subtract operation
    //As a user , ehrn I enter 6 and 5 as input , the result be 1
    @Test
    public void gibnAandBWhenSubtractTennC(){
        int a=5;
        int b=6;
        int c=1;

        int result =calculator.subtract(a,b);

        assertThat(result).isEqualTo(c);

    }
}