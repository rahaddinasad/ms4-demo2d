package az.raheddin.ms4demo2d.repository;


import az.raheddin.ms4demo2d.model.Student;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StudentRepository extends CrudRepository<Student,Long> {
    List<Student> findByInstitute(String institute);
}
