package az.raheddin.ms4demo2d.config;

import az.raheddin.ms4demo2d.beans.StudentBeans;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfig {

    public  String Name;

    public  String Version;

    public List<String> developers;

    public Map<String, StudentBeans> tags;
}
