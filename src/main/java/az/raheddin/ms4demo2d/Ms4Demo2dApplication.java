package az.raheddin.ms4demo2d;

import az.raheddin.ms4demo2d.beans.StudentBeans;
import az.raheddin.ms4demo2d.config.AppConfig;
import az.raheddin.ms4demo2d.service.TicTacToe;
import az.raheddin.ms4demo2d.service.TicTacToeDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class Ms4Demo2dApplication implements CommandLineRunner {

	public final StudentBeans student;

	public final AppConfig appConfig;

	private final TicTacToe ticTacToe;



	public static void main(String[] args) {
		SpringApplication.run(Ms4Demo2dApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	//	System.out.println(student);
		/*System.out.println(appConfig.Name +" "+appConfig.Version + " " + appConfig.developers);

		appConfig.developers.stream().forEach(System.out::println);
		appConfig.tags.forEach((k,v)-> System.out.println(k +"= "+v));*/
		ticTacToe.play(1,2);
	}
}
