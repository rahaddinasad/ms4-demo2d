package az.raheddin.ms4demo2d.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/hello")
public class HelloController {
    @GetMapping("/{username}")
    public ResponseEntity<String> getById(@PathVariable String username) {
        log.trace("Path variable username: {}", username);
        return ResponseEntity.ok("Hello " + username);
    }

}
