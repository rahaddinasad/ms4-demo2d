package az.raheddin.ms4demo2d.controller;

import az.raheddin.ms4demo2d.service.TicTacToe;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class TicTacToeControler {

    private final TicTacToe game;

    @PostMapping("/{x}/{y}")
    public String play(@RequestParam int x,@RequestParam int y) {
        return game.play(x,y);
    }

    }
