package az.raheddin.ms4demo2d.controller;

import az.raheddin.ms4demo2d.model.Student;
import az.raheddin.ms4demo2d.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {


    private final StudentService studentService;

    @PostMapping()  //201
    public ResponseEntity<Student> save(@Validated @RequestBody Student student) {
        log.trace("save student : {}", student);
        return ResponseEntity.status(HttpStatus.CREATED).body(studentService.save(student));
    }

    @GetMapping("/{id}") //200
    public Student students(@PathVariable Long id) {
        log.trace("students");
        return studentService.getStudent(id);
    }

    @DeleteMapping("/{id}") //204
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.trace("delete Student : {}", id);
        studentService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/{id}") //200
    public Student uppdate(@PathVariable Long id, @RequestBody Student student) {
       return studentService.uppdate(id, student);
    }

}
