package az.raheddin.ms4demo2d.beans;


import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component //singleton
public class StudentBeans {

    private static StudentBeans student;

    private String name;

    private String lastName;
/*

    public static Student getInstance() {
        if (student == null) {
            synchronized (Student.class) {
                if (student == null)
                    student = new Student();
            }
        }
        return student;
    }

*/

}
