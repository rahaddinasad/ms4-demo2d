package az.raheddin.ms4demo2d.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TicTacToeDAO implements ITicTacToeDAO{
    @Override
    public TicTacToeBean getLastMove() {
        log.info("This is reall implementation");
        return new TicTacToeBean(1,'X',1,2);
    }

    @Override
    public void saveMove(TicTacToeBean ticTacToeBean) {
        log.info("This is reall implementation");
     //save
    }
}
