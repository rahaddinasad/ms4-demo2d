package az.raheddin.ms4demo2d.service;

public interface ITicTacToeDAO {

      TicTacToeBean getLastMove();

     void saveMove(TicTacToeBean ticTacToeBean) ;
}
