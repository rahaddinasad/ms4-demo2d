package az.raheddin.ms4demo2d.service;

public class Calculator {

    public int add(int a, int b) {
        return addPrivate(a,b);
    }

    public int subtract(int a, int b) {
        return b-a;
    }

    private int addPrivate(int a, int b) {
        return a+b;
    }
}
