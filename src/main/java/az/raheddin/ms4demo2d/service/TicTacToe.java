package az.raheddin.ms4demo2d.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TicTacToe {

    private char lastPlayer = '\0';

    private final ITicTacToeDAO dao;

    private static final int SIZE = 3;

    private Character[][] board = {
            {'\0', '\0', '\0'},
            {'\0', '\0', '\0'},
            {'\0', '\0', '\0'}
    };

    public String play(int x, int y) { //x coordinate, y coordinate
        checkAxis(x); //validate
        checkAxis(y); //validate
        lastPlayer = nextPlayer();
        setBox(x, y, lastPlayer); //board
        saveMove(x,y);
        if (isWin(x,y)) {
            return lastPlayer + " is the winner";
        } else if (isDraw()) {
            return "The result is draw";
        }else
        return "No winner";
    }

    private boolean isWin(int x, int y) {
        int playerTotal = lastPlayer * 3;
        char horizontal, vertical, diagonal1, diagonal2;
        horizontal = vertical = diagonal1 = diagonal2 = '\0';
        for (int i = 0; i < SIZE; i++) {
            horizontal += board[i][y - 1];
            vertical += board[x - 1][i];
            diagonal1 += board[i][i];
            diagonal2 += board[i][SIZE - i - 1];
        }
        if (horizontal == playerTotal
                || vertical == playerTotal
                || diagonal1 == playerTotal
                || diagonal2 == playerTotal) {
            return true;
        }

        return false;
    }

    private void setBox(int x, int y, char lastPlayer) {
        checkIfBoardIsOccupied(x, y);
        board[x - 1][y - 1] = lastPlayer;
    }

    private void checkAxis(int coordinate) {
        if (coordinate < 1 || coordinate > 3) {
            throw new RuntimeException("X is outside board");
        }
    }

    private void checkIfBoardIsOccupied(int x, int y) {
        if (board[x - 1][y - 1] != '\0') {
            throw new RuntimeException("Box is occupied");
        }
    }

    public Character nextPlayer() {
        return lastPlayer == 'X' ? 'O' : 'X';
    }

    private boolean isDraw() {
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                if (board[x][y] == '\0') {
                    return false;
                }
            }
        }
        return true;
    }


    private void saveMove(int x, int y) {
     TicTacToeBean lastMove = dao.getLastMove();
     dao.saveMove(new TicTacToeBean(lastMove.getMove()+1,lastPlayer,x,y));
    }
}
