package az.raheddin.ms4demo2d.service;

import az.raheddin.ms4demo2d.model.Student;
import az.raheddin.ms4demo2d.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@RequiredArgsConstructor
public class StudentService {


    private final StudentRepository studentRepository;

    public  Student getStudent(Long id){
        return studentRepository.findById(id).orElseThrow(()->new RuntimeException("Student not found"));
    }

    public Student save(Student student){
       return studentRepository.save(student);
    }

    public void delete(Long id){
        studentRepository.deleteById(id);
    }

    public Student uppdate(Long id, @RequestBody Student student){
         studentRepository.findById(id).orElseThrow(()->new RuntimeException("Student not found"));
         student.setId(id);
         return studentRepository.save(student);
    }
}
